// FOS Common

LOG_MODULE_COMPILE

#pragma enum CType DEFAULT_M = 0
#pragma enum CType DEFAULT_F = 4
#pragma enum CType DEFAULT_IT = 148

#pragma property Critter PrivateServer int BaseCrType
#pragma property Critter PrivateServer int CrType

#ifdef __CLIENT_SERVER
#pragma rpc Server rpc_ChangeCrType(string, uint, int) // name, id, crtype
#endif



void ModuleInit()
{
	 Initialize();
}

enum AnimationType
{
   	Fallout = 0,
	VanBuren3d = 1,
	Tactics = 2,
	Arcanum = 3,
	Rebirth = 4
};

//Начинается с #, поскольку перечисление Anim1 начинается с 1, не с 0.
const string AnimationsAll = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

class CritterType
{
	int 	CritterTypeId;
	int		AnimType;
	string 	Name;
	hash	Proto;
	int 	Alias;
	uint 	Multihex;
	bool 	CanWalk;
	bool 	CanRun;
	bool 	CanAim;
	bool 	CanChangeArmor;
	bool 	CanRotate;
	/// Walk time in milliseconds.
	int		WalkTime;
	/// Run time in milliseconds.
	int		RunTime;
    /// Walk steps (Fallout specific) - frames per hex for walking, for running used 1-2, 3-4, 5-7, 8-end;
	/// пример: 4  9 11  0
	/// 4 - № кадра анимации, который завершает переход из первого гекса во второй
	/// 9 - № кадра анимации, который завершает переход из второго гекса в третий
	/// 11 - № кадра анимации, который завершает переход из третьего гекса во четвертый
	/// 0 - № кадра анимации, который завершает переход из четвертого гекса в пятый
	int[] 	WalkSteps;   
	///  Move (User types specific) - first value - frames per hex walk, second - frames per hex run, if zero than taked default values 5 (walk) and 2 (run).
	int MoveWalkFrames;
	int MoveRunFrames;
	/// Доступные для типа криттера анимации. Склеены в строку
	/*
	# Primary animations
	# 1  A Unarmed
	# 4  D Knife
	# 5  E Club
	# 6  F Hammer
	# 7  G Spear
	# 8  H Pistol
	# 9  I SMG
	# 10 J Shootgun
	# 11 K Heavy Rifle
	# 12 L Minigun
	# 13 M Rocket Launcher
	# 14 N Flamer
	# 15 O Rifle
	# 16 P Sword
	# 17 Q Long Sword
	# 18 R Axe
	# 19 S Bow
//enum Anim1 Unarmed = 1 совпадает с индексом анимаций
	*/
	string Animations;
	
	bool HasAnim( uint weaponAnim )
	{
		for( uint i = 0, l = Animations.length(); i < l; i++ )
		{
			if( Animations[ i ] == AnimationsAll[ weaponAnim ] )
				return true;
		}
		return false;
	}
	
	CritterType AliasType()
	{
		return GetCritterType( Alias );
	}
}

dict<int, CritterType@> CritterTypes = {};
bool Initialized = false;

void Initialize()
{
	if(!Initialized)
	{
		Log("Initializing Critter types...");
		#ifdef __SERVER
		Json::JSON@ info = Json::JSONLoad("Data/CritterTypes.json");
		#endif
		#ifndef __SERVER
		#message "move data from file into somewhere where clients just cant easily access it"
		Json::JSON@ info = Json::JSONLoad("CritterTypes.json");
		#endif
		info.forEach( @ReadCritterTypeInfo );
		Initialized = true;

		#ifdef __CLIENT
		RegisterCommands();
		#endif
		Log("Critter types initialized.");
	}
}

void ReadCritterTypeInfo(Json::JSON@ crInfo, uint index)
{
	CritterType@ ct = @CritterType();
	string modelPath = "";
	string modelName = "";
	crInfo[ "CritterTypeId" ] >> ct.CritterTypeId;
	crInfo[ "AnimationType" ] >> ct.AnimType;
	crInfo[ "AliasTypeId" ] >> ct.Alias;
	crInfo[ "CritterTypeName" ] >> modelPath;
	string targetModel = Utils::Path::GetPathTarget(modelPath);
	if(Strings::ValidString(targetModel))
	{
		string extension = Utils::Path::GetExtension(modelPath);
		if(Strings::ValidString(extension))
		{
			Log("Extension " + extension);
			targetModel = Utils::Path::RemoveExtension(targetModel);
		}
		modelName = targetModel;
	}
	else
	{
		Log("Target model ( name/path ) is empty, fix !");
		return;
	}
	ct.Name  = modelName;
	//Log("Parsing" + " " + modelName + " " + "with path " + modelPath);
	ct.Proto = HASH( modelPath );
	crInfo[ "CanWalk" ] >> ct.CanWalk;
	crInfo[ "CanRun" ] >> ct.CanRun;
	crInfo[ "CanAim" ] >> ct.CanAim;
	crInfo[ "CanChangeArmor" ] >> ct.CanChangeArmor;
	crInfo[ "CanRotate" ] >> ct.CanRotate;
	crInfo[ "WalkTime" ] >> ct.WalkTime;
	crInfo[ "RunTime" ] >> ct.RunTime;
	crInfo[ "MoveWalkFrames" ] >> ct.MoveWalkFrames;
	crInfo[ "MoveRunFrames" ] >> ct.MoveRunFrames;
	crInfo[ "Animations" ] >> ct.Animations;
	ct.WalkSteps = array< int >( 4 );
	crInfo[ "WalkSteps" ] >> ct.WalkSteps;
	
	AddCritterType(ct);
}


void AddCritterType(CritterTypes::CritterType@ ctin)
{
	int id = ctin.CritterTypeId;
	AddCritterType(ctin, id);
}

void AddCritterType(CritterTypes::CritterType@ ctin, int id)
{
	if(CritterTypes.exists(id))
	{
		CritterTypes.remove(id);
	}
	CritterTypes.set(id, @ctin);
}


CritterTypes::CritterType@ GetCritterType(int id)
{
	CritterType@ existing = null;
	if(CritterTypes.exists(id))
	{
		@existing = CritterTypes.get(id);
	}
	return existing;
}

#ifdef __CLIENT 

// string critter name OR uint critterId AND int crtypeId
// -ct for crtype
// -id for crid
// -n for playername ( players only )
bool CmdChangeCrType(array<Command::CommandArg@>@ args)
{
	if(valid ( args ) )
	{
		string name 	= "";
		uint crId 		= 0;
		int  crType 	= -1;
		bool allow 		= false;

		Command::CommandArg@ crtypeId = Command::GetArg( args, "-ct" );

		if( valid ( crtypeId ) )
		{
			// this causes faulty bytecode on client side, for some reason
		//	bool convert = Strings::StrToInt(crtypeId.value, crType);
			bool convert = Strings::Str2Int( crtypeId.value, crType );
			if( convert )
			{
				Command::CommandArg@ nameOrId = Command::GetArg( args, "-id" );
				if( !valid ( nameOrId ) )
				{
					nameOrId = Command::GetArg( args, "-n" );
					if( valid ( nameOrId ) )
					{
						name = nameOrId.value;
						allow = true;
					}
				}
				else
				{
					convert = Strings::Str2Uint( nameOrId.value, crId );
					if( convert )
					{
						allow = true;
					}
				}
			}

			if( allow )
			{
				::Message("Allowed, args are " + name + " " + crId + " " + crType );
				ServerRpc.rpc_ChangeCrType(name, crId, crType);
			}
		}
		return allow;
	}
	return false;
}

void RegisterCommands()
{
	RegisterRpcCommands();
}

void RegisterRpcCommands()
{
	Command::Command@ ChangeCrTypeCommand = @Command::Command("ccrtype", @CmdChangeCrType, ACCESS_MODER);
	ChangeCrTypeCommand.set_ManMsg(STR_CMD_MAN_CCRTYPE);

	client_commands::RegisterRpcCommand(@ChangeCrTypeCommand);
}

#endif

#ifdef __SERVER

void SetCritterType(Critter cr, int ctId)
{
	CritterType@ existing = GetCritterType(ctId);
	if(valid(existing))
	{
		cr.ModelName = existing.Proto; // what the fuck ?
		cr.CrType = ctId;
	}
	else
		Log("Cannot set critter type with id of " + ctId + " not found.");
}

void SetBaseCritterType(Critter& cr, int ctId)
{
	CritterType@ existing = GetCritterType(ctId);
	if(valid(existing))
	{
		// todo add base crtype field and put assigment mech here
		cr.BaseCrType = ctId;
	}
	else
		Log("Cannot set critter type with id of " + ctId + " not found.");
}

void rpc_ChangeCrType(Critter sender, string targetName, uint targetId, int crtypeId)
{
	
	int access = sender.GetAccess();
	if( access >= ACCESS_MODER )
	{
		sender.Say(SAY_NETMSG, "Setting target critter crtype to " + crtypeId);
		Critter@ targetCr = null;
		if( targetId != 0)
		{
			targetCr = ::GetCritter( targetId );
		}
		else
		if( targetName.length() > 0 )
		{
			// player only, by name
			const Critter@ playerPtr = ::GetPlayer( targetName );
			if( valid( playerPtr ) )
			{
				#message "Cvet have mercy"
				targetCr = ::GetCritter( playerPtr.Id ); // what the fuck cvet srsly
			}
		}

		if( valid ( targetCr ) ) 
		{
			SetCritterType( targetCr , crtypeId );
		}
	}
}

#endif

/* CritterTypes::CritterType@ GetCritterType(Critter& cr)
{
	int id = cr.CrType;
	return GetCritterType(id);
}
 */


# ifndef __MAPPER
/* uint CritterGetCrTypeAlias( const Critter cr )
{
	if( cr.IsPlayer() )
	{
		auto crType = GetCritterType( cr );
		if( valid( crType ) && crType.Alias > 0)
			return crType.Alias;
	}
	return cr.CrTypeAliasBase;
}
 */
# endif
