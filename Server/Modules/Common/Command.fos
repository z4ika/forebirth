// FOS Common Sort -1
#ifndef __COMMAND_MODULE__
#define __COMMAND_MODULE__

LOG_MODULE_COMPILE

#ifdef __CLIENT_SERVER
// client doesnt have access natively, lets fix that
#pragma property Critter Protected int ScriptAccess
#endif

#define COMMAND_LOCAL_START_SIGN  	("!")
#define COMMAND_REMOTE_START_SIGN 	("`") // grave
#define COMMAND_DELIMETER  			(" ")
#define COMMAND_ARG_PREFIX 			("-")

#ifdef __SERVER
funcdef bool CommandFunc(Critter@ sender, array<CommandArg@>@ args);
#endif

#ifndef __SERVER
funcdef bool CommandFunc(array<CommandArg@>@ args);
#endif

shared class Command
{
	CommandFunc@ handler = null;
	string name = "unnamed";

	#ifndef __SERVER
	uint manMsg = 0;
	#endif

	#ifndef __MAPPER
	int accessLevel;
	
	Command(string& name, CommandFunc@ handler, int accessLevel)
	{
		this.set_Name(name);
		this.set_Handler(handler);
		this.accessLevel = accessLevel;
	}
	#endif

	#ifdef __MAPPER
	Command(string& name, CommandFunc@ handler)
	{
		this.set_Name(name);
		this.set_Handler(handler);
	}
	#endif


	#message "Do not use default command constructor by any means"
	Command()
	{
		#ifdef __SERVER
		Log("Creating command with default argument ! DONT !");
		#endif
		#ifdef __CLIENT_MAPPER
		Message("Creating command with default constructor, DONT !");
		#endif
	}
	
	string get_Name()
	{
		return this.name;
	}
	
	CommandFunc@ get_Handler()
	{
		return this.handler;
	}
	
	void set_Name(string& name)
	{
		this.name = name;
	}
	
	void set_Handler(CommandFunc@ handler)
	{
		@this.handler = @handler;
	}

	#ifdef __CLIENT_MAPPER
	uint get_ManMsg()
	{
		return this.manMsg;
	}
	
	void set_ManMsg(uint newNum)
	{
		this.manMsg = newNum;
	}
	#endif

	#ifndef __MAPPER
	int get_Access()
	{
		return this.accessLevel;
	}
	
	void set_Access(int newAcc)
	{
		this.accessLevel = newAcc;
	}
	#endif

	#ifdef __CLIENT_SERVER
	bool CheckAccess(int access)
	{
		bool result = access >= this.get_Access();
		return result;
	}
	#endif

	#ifdef __SERVER
	bool CheckAccess(Critter& sender)
	{
		int crAcc = sender.GetAccess();
		int cmdAcc = this.get_Access();
		return crAcc >= cmdAcc;
	}
	#endif

	#ifdef __CLIENT
	bool CheckAccess()
	{
		Critter@ chosen = ::GetChosen();
		bool result = false;
		if( valid ( chosen ) )
		{
			int access = chosen.ScriptAccess;
			int cmdAccess = this.get_Access();
			result = ( access >= cmdAccess );
		}
		return result;
	}
	#endif

	#ifdef __CLIENT_MAPPER
	void PrintManual()
	{
		uint manMsg = this.get_ManMsg();
		string manText = ::GetMsgStr( TEXTMSG_TEXT, manMsg );
		::Message("Usage for " + this.get_Name() + " : " + manText );
	}
	#endif

	
	#ifdef __SERVER
	void Run(Critter@ sender, array<CommandArg@>@ args)
	#endif
	#ifndef __SERVER
	void Run(array<CommandArg@>@ args)
	#endif
	{
		#ifdef __DEBUG
		bool validargs = valid(args);
		Log("Running command " + this.name + " with args being " + (validargs ? "not null" : "null"));
		if(validargs)
		{
			uint argc = args.length(); // pair of argname and argvalue ( value can be empty, i.e a switch or smth )
			for(uint i = 0; i < argc; i++)
			{
				Log("arg " + i + " is " + args[i].ToString());
			}
		}
		#endif

		if(valid(this.handler))
		{
			#ifdef __SERVER
			if( !this.handler(sender, args) )
			{

			}
			#endif
			
			#ifndef __SERVER
			if( !this.handler(args) )
			{
				this.PrintManual();
			}
			#endif
		}
	}
}

// maybe make it dict ?
shared class CommandArg
{
	string name  = "";
	string value = "";
	
	CommandArg(string& name, string& value)
	{
		this.name = name;
		this.value = value;
	}

	string GetValue()
	{
		return this.value;
	}

	string GetName()
	{
		return this.name;
	}
	
	string ToString()
	{
		return this.name + " " + this.value;
	}
}

shared class CommandManager
{
	string name = "default";
	dict<string, Command@>@ commands = {};
	
	CommandManager(string& name)
	{
		this.set_Name(name);
	}
	
	string get_Name()
	{
		return this.name;
	}
	
	void set_Name(string& name)
	{
		this.name = name;
	}
	
	#ifdef __SERVER
	void ExecuteCommand(Critter@ sender, string& rawCommand)
	#endif
	#ifndef __SERVER
	void ExecuteCommand(string& rawCommand)
	#endif
	{
		array<string>@ parsedCommand = this.ParseCommand(rawCommand);
		if(valid(parsedCommand))
		{
			string commandName = parsedCommand[0]; // first is always command name;
			Command@ existing = this.GetCommand( commandName );
			if( valid ( existing ) )
			{

				#ifdef __CLIENT_SERVER

				bool access = true;
				#ifdef __SERVER
				access = (valid(sender) ? existing.CheckAccess(sender) : true);
				#endif

				#ifdef __CLIENT
				access = existing.CheckAccess();
				#endif

				if( !access )
				{
					#ifdef __CLIENT
					::Message("Access denied.");
					#endif

					return; // dont process, access denied.
				}

				#endif

				
				parsedCommand.removeAt(0);
				array<CommandArg@>@ args = this.ParseArgs(parsedCommand);
				
				#ifdef __SERVER
				existing.Run(sender, args);
				#endif
				
				#ifndef __SERVER
				existing.Run(args);
				#endif
			}
			else
			{
				#ifdef __DEBUG
					#ifdef __SERVER
					Log("[" + this.name + "] " + "Command " + commandName + " not found.");
					#endif
					
					#ifndef __SERVER
						Message("[" + this.name + "] " + "Command " + commandName + " not found.");
					#endif
				#endif			
			}
		}
	}
	
	array<CommandArg@>@ ParseArgs(array<string>@ arglist)
	{
		uint argslen = arglist.length();
		if(argslen > 0)
		{
			array<CommandArg@>@ args = @array<CommandArg@>();
			for(uint i = 0; i < argslen; i++)
			{
				string cur = arglist[i];
				if(cur.startsWith(COMMAND_ARG_PREFIX))
				{
					CommandArg@ newArg = @CommandArg();
					newArg.name = cur;
					args.insertLast(newArg);
					if(i+1 < argslen) // if there's something else
					{
						cur = arglist[i+1];
						if(!cur.startsWith(COMMAND_ARG_PREFIX)) // not an arg but value
						{
							newArg.value = cur;
							i++;
							continue;
						}
					}
				}
			}
			return args;
		}
		return null;
	}
	
	array<string>@ ParseCommand(string& rawCommand)
	{
		array<string>@ splitted = rawCommand.split(COMMAND_DELIMETER);
		return splitted;
	}
	
	CommandManager@ AddCommand(Command@ command)
	{
		if(valid(command))
		{
			this.commands.set(command.get_Name(), @command);
		}
		return this;	
	}
	
	CommandManager@ AddCommand(Command@ command, string& name)
	{
		if(valid(command))
		{
			command.set_Name(name);
			this.commands.set(name, @command);
		}
		return this;
	}
	
	CommandManager@ RemoveCommand(Command& command)
	{
		string nameKey = command.get_Name();
		return this.RemoveCommand(nameKey);
	}
	
	CommandManager@ RemoveCommand(string& name)
	{
		if(this.commands.exists(name))
		{
			this.commands.remove(name);
		}
		return this;
	}

	array < Command@ >@ GetAllCommands()
	{
		uint cmdCount = this.commands.length();
		array < Command@ >@ result = null;
		if(cmdCount > 0)
		{
			@result = @array < Command@ > ( cmdCount );
		}
		for(uint i = 0; i < cmdCount; i++)
		{
			@result[i] = this.commands.getValue(i);
		}

		return result;
	}

	Command@ GetCommand(string& name)
	{
		Command@ result = null;
		if( this.commands.exists( name ) )
		{
			result = this.commands.get( name );
		}
		return result;
	}

}

shared CommandArg@ GetArg(array<CommandArg@>@ arglist, string& argname)
{
	uint argc = arglist.length();
	for(uint i = 0; i < argc; i++)
	{
		CommandArg@ cur = arglist[i];
		if(cur.name == argname)
		{
			return cur;
		}
	}
	return null;
}


#endif