//FOS Client Server

LOG_MODULE_COMPILE

#ifdef __CLIENT_SERVER

#pragma rpc Server rpc_MoveInvItem ( uint itemId, uint8 toSlot )
#pragma rpc Server rpc_DropInvItem ( uint itemId, uint count   )
#pragma rpc Server rpc_PickItem	   ( uint itemId, uint count   )

#endif // __CLIENT_SERVER

#ifdef __CLIENT

shared interface IChosenAction
{
	// return true when action is done performing, otherwise false - next frame retry
	bool Perform(); // virtual
}


array< IChosenAction@ >  AllActions  = {};

void ModuleInit()
{
	__EventLoop.Subscribe( ChosenProcess );
    __EventMapLoad.Subscribe( OnMapChange );
    __EventMapUnload.Subscribe( OnMapChange );
}

// drop actions when map changes
void OnMapChange()
{
    StopAllActions(); 
}

uint GetChosenActions( array< IChosenAction@ > actions )
{
    if( valid( actions ) )
        actions = AllActions;

    return AllActions.length();
}

void SetChosenActions( array< IChosenAction@ >@ actions )
{
    AllActions.resize( 0 );

    if( valid ( actions )  )
    {
        AllActions = actions.clone();
    }
}

void SetAction( IChosenAction@ action, bool append, bool toBeginning )
{
	if( valid ( action ) )
	{
		if( !append )
       	 AllActions.resize( 0 );

	    if( toBeginning )
	    {
			AllActions.insertFirst( action );
	    }
	    else
	    {
	    	AllActions.insertLast( action );
	    }
	}
}


void StopAllActions()
{
    if( AllActions.length() == 0 )
        return;

    AllActions.clear();
}

#message "finish chosen actions implementation"
void ChosenProcess()
{
	//todo
	Critter chosen = GetChosen();
	if( valid( chosen ) )
	{

		if( valid( CurMap ) )
	    {
	        // Roof
	        client_customcall::SkipRoof( chosen.HexX, chosen.HexY );    //( "SkipRoof " + chosen.HexX + " " + chosen.HexY );

	        // Hidden mode
	        client_customcall::SetChosenAlpha( chosen.IsHide ? 130 : 255 ); //( "ChosenAlpha " + ( chosen.IsHide ? 130 : 255 ) );

	    //    // Shoot borders
	    //    client_customcallCustomCall( "SetShootBorders " + ( __Cursor == CursorType::UseWeapon ? "true" : "false" ) );
	    }

	    // Actions
	    if( !chosen.IsFree() )
	        return;

	    if( AllActions.length() == 0 )
        	return;

	    if( !chosen.IsLife() )
  		{
      	 	StopAllActions();
      		return;
    	}


    	IChosenAction@ currentAction = AllActions[0];

    	//perform action if exists
    	bool performed = currentAction.Perform();

    	if( performed )
    	{
    		// remove front action
	   	  	if( AllActions.length() > 0 )
		  	{
		  		AllActions.removeFirst();
		  	}
    	}
	}
}

void SetMoveAction(uint16 hx, uint16 hy, bool run, uint cut)
{
	CChosenAction_Move new_action = CChosenAction_Move( hx, hy, run, cut );
	SetAction( new_action, false, true );
}

void SetMoveInvItemAction(uint itemId, uint8 toSlot)
{
	CChosenAction_MoveInvItem new_action = CChosenAction_MoveInvItem( itemId, toSlot );
	SetAction( new_action, false, true );
}

void SetDropInvItemAction( uint itemId, uint count )
{
	CChosenAction_DropInvItem new_action = CChosenAction_DropInvItem ( itemId, count );
	SetAction( new_action, false, true );
}

void SetPickUpItem( uint itemId, uint count )
{
	CChosenAction_PickItem new_action = CChosenAction_PickItem ( itemId, count);
	SetAction( new_action, false, true );
}

void SetUseItem( uint itemId )
{

}

void SetUseItemOnCritter( uint crId, uint itemId )
{

}

void SetUseItemOnItem( uint targetId, uint itemId )
{

}

void SetUseSKill( CritterProperty skNum )
{
	CChosenAction_UseSkill new_action = CChosenAction_UseSkill ( skNum );
	SetAction( new_action, false, true );
}

void SetUseSkillOnCritter( uint crId, CritterProperty skNum)
{
	CChosenAction_UseSkillOnCritter new_action = CChosenAction_UseSkillOnCritter( crId, skNum );
	SetAction( new_action, false, true );
}

void SetUseSkillOnItem( uint itemId, CritterProperty skNum)
{
	CChosenAction_UseSkillOnItem new_action = CChosenAction_UseSkillOnItem( itemId, skNum );
	SetAction( new_action, false, true );
}


class CChosenAction_UseSkill : IChosenAction
{
	CritterProperty skNum;

	CChosenAction_UseSkill( CritterProperty skNum )
	{
		this.skNum = skNum;
	}

	bool Perform() override
	{
		return true;
	}
}

class CChosenAction_UseSkillOnCritter : IChosenAction
{
	uint crId;
	CritterProperty skNum;

	CChosenAction_UseSkillOnCritter( uint crId, CritterProperty skNum )
	{
		this.crId  = crId;
		this.skNum = skNum;
	}

	bool Perform() override
	{
		return true;
	}
}

class CChosenAction_UseSkillOnItem : IChosenAction
{
	uint itemId;
	CritterProperty skNum;

	CChosenAction_UseSkillOnItem( uint itemId, CritterProperty skNum )
	{
		this.itemId  = itemId;
		this.skNum 	 = skNum;
	}

	bool Perform() override
	{
		return true;
	}
}


class CChosenAction_PickItem : IChosenAction
{
	uint itemId;
	uint count;

	CChosenAction_PickItem( uint itemId, uint count )
	{
		this.itemId = itemId;
	}

	bool Perform() override
	{
		Critter@ chosen = ::GetChosen();
		if(!valid ( chosen ) )
		{
			::Log("CChosenAction_PickItem::Perform -> Chosen is not valid.");
			return true;
		}

		if(!valid (CurMap) )
		{
			::Log("CChosenAction_PickItem::Perform -> Chosen action is not valid.");
			return true;
		}

		Item@ targetItem = ::GetItem( itemId );
		if(!valid ( targetItem ) )
		{
			::Log("CChosenAction_PickItem::Perform -> Target item is not valid.");
			return true;
		}

		// check path
		if( !CheckPath( chosen.HexX, chosen.HexY, targetItem.HexX, targetItem.HexY, 0 ) )
			return true;

		// check distantion
		if( !CheckDistantion( chosen.HexX, chosen.HexY, targetItem.HexX, targetItem.HexY, 1 ) )
		{
			// we're too far away from target
			// put a new action in front of current action
			SetAction( CChosenAction_Move( targetItem.HexX, targetItem.HexY, true, 0 ), true, true );
			return false;
		}

		// check script allow
		bool allow = __EventItemCheckMove.Raise( targetItem, count, CurMap, chosen );
		if(!allow)
			return true;

		ChosenActions::ServerRpc.rpc_PickItem( itemId, count );
		__EventCritterAction.Raise ( true, chosen, ACTION_PICK_ITEM, 0, targetItem );
		return true;
	}
}

class CChosenAction_DropInvItem : IChosenAction
{
	uint itemId;
	uint count;

	CChosenAction_DropInvItem( uint itemId, uint count )
	{
		this.itemId = itemId;
		this.count  = count;
	}

	bool Perform() override
	{
		Critter@ chosen = ::GetChosen();
		if(!valid ( chosen ) )
		{
			::Log("CChosenAction_DropInvItem::Perform -> Chosen is not valid.");
			return true;
		}

		Item@ targetItem = chosen.GetItem( this.itemId );
		if(!valid( targetItem ) ) 
		{
			::Log("CChosenAction_DropInvItem::Perform -> Targe item is not valid.");
			return true;
		}

		bool allowed = __EventItemCheckMove.Raise( targetItem, this.count, chosen, CurMap );
		if( !allowed )
			return true;

		ChosenActions::ServerRpc.rpc_DropInvItem( this.itemId, this.count );
		__EventCritterAction.Raise( true, chosen, ACTION_DROP_ITEM, 0, targetItem );
		return true;
	}
}

class CChosenAction_MoveInvItem : IChosenAction
{
	uint itemId;
	uint8 toSlot;

	CChosenAction_MoveInvItem( uint itemId, uint8 toSlot )
	{
		this.itemId = itemId;
		this.toSlot = toSlot;
	}

	bool Perform() override 
	{

		Critter@ chosen = ::GetChosen();
		if(!valid ( chosen ) ) 
		{
			::Log("CChosenAction_MoveInvItem::Perform -> Chosen is not valid.");
			return true;
		}

		Item@ targetItem = chosen.GetItem( this.itemId );
		if(!valid( targetItem ) )
		{
			::Log("CChosenAction_MoveInvItem::Perform -> Target item is not valid to move.");
			return true;
		}

		if( targetItem.CritSlot == this.toSlot )
			return true; 

		bool allowed = __EventCritterCheckMoveItem.Raise( chosen, targetItem, this.toSlot );
		if( !allowed )
		{
			::Log("CChosenAction_MoveInvItem::Perform -> Target item is not allowed to move to slot " + toSlot );
			return true;
		}

		// if were trying to put shid into non-inv slots = one item slots basically
		if( this.toSlot != SLOT_INV )
		{
			Item@ slotItem = chosen.GetItemBySlot( this.toSlot );
			if(valid( slotItem ) )
			{
				allowed = __EventCritterCheckMoveItem.Raise( chosen, targetItem, SLOT_INV ); // if we can move item in slot back to inv
				if( !allowed )
				{
					::Log("CChosenAction_MoveInvItem::Perform -> Desired slot contains an item that couldnt be moved to inventory.");
					return true;
				}
				else
				{
					// put a new action in front of current action
					SetAction( CChosenAction_MoveInvItem( slotItem.Id, SLOT_INV ), true, true );
					return false; // not finished
				}
			}
		}

		ChosenActions::ServerRpc.rpc_MoveInvItem( this.itemId, this.toSlot );
		__EventCritterAction.Raise( true, chosen, ACTION_MOVE_ITEM, 0, targetItem );
		return true;
	}
}

class CChosenAction_Move : IChosenAction
{
	uint16 hx;
	uint16 hy;
	bool run;
	uint cut;
	array< uint8 > movePath  = {};

	CChosenAction_Move(uint16 hx, uint16 hy, bool run, uint cut)
	{
		this.hx = hx;
		this.hy = hy;
		this.run = run;
		this.cut = cut;
	}

	// when destructed i.e if removed from queue externally
	~CChosenAction_Move()
	{
		// send move end
		if(this.movePath.length() > 0)
  		{
        	CustomCall( "SendMove" );
  		}
 	}
	

	bool Perform() override
	{
		Critter chosen = ::GetChosen();

		bool findPath = true;

		if( findPath && ::GetDistantion( chosen.HexX, chosen.HexY, this.hx, this.hy ) <= this.cut )
    		findPath = false;

    	// Find path
        if( findPath )
        {
            uint8[]   path = ::GetPath( chosen.HexX, chosen.HexY, this.hx, this.hy, this.cut );
            if( !valid( path ) && ::GetDistantion( chosen.HexX, chosen.HexY, this.hx, this.hy ) > this.cut + 1 )
                 path = ::GetPath( chosen.HexX, chosen.HexY, this.hx, this.hy, ++this.cut );

            if( valid( path ) )
                this.movePath = path;
            else
                this.movePath.resize( 0 );
        }


        uint pathLen = movePath.length();

        // Transit
        if( pathLen > 0 )
        {
            uint16 nextHx = chosen.HexX;
            uint16 nextHy = chosen.HexY;
            MoveHexByDir( nextHx, nextHy, this.movePath[ 0 ], 1 );
            client_customcall::TransitCritter( nextHx, nextHy, true, false); //CustomCall( "TransitCritter " + nextHx + " " + nextHy + " true false" );
            client_customcall::SetCursorPos();		 //CustomCall( "SetCursorPos" );
            client_customcall::RebuildLookBorders(); // ( "RebuildLookBorders" );

            // Send about move
	        string moveStr = "";
	        
	        for( uint i = 0; i < pathLen; i++ )
	            moveStr += " " + this.movePath[ i ];
	        CustomCall( "SendMove" + moveStr );
	        ::Log( moveStr );

	        this.movePath.removeAt( 0 );
        }

        // Continue move
        if( this.movePath.length() > 0 )
           	return false;
        else
			return true;
	}
}


bool CheckPath(uint16 hx, uint16 hy, uint16 tx, uint16 ty, uint cut)
{
	// check path
	uint8[]	path = ::GetPath( hx, hy, tx, ty, cut ); // cut path by size of critter
	if( !valid( path ) && CheckDistantion( hx, hy, tx, ty, cut+1 ) )
	{
		@path = @::GetPath( hx, hy, tx, ty, ++cut );;
		if( !valid( path ) )
			return false;
	}

	return true;
}

bool CheckDistantion(uint16 hx, uint16 hy, uint16 tx, uint16 ty, uint threshold)
{
	uint dist = ::GetDistantion( hx, hy, tx, ty );
	if(dist > threshold)
	{
		return false;
	}
	return true;
}

#endif // __CLIENT


#ifdef __SERVER

void rpc_MoveInvItem( Critter sender, uint itemId , uint8 toSlot )
{
	CritterAction::MoveInvItem( sender, itemId, toSlot );
}

void rpc_DropInvItem( Critter sender, uint itemId , uint count )
{
	CritterAction::DropInvItem( sender, itemId, count );
}

void rpc_PickItem( Critter sender, uint itemId, uint count)
{
	CritterAction::PickItem( sender, itemId, count);
}

#endif // __SERVER
