// FOS Common

#ifndef DYNAMIC_OBJECT
#define DYNAMIC_OBJECT

shared interface IAny
{
	bool IsInt();
	bool IsString();
	bool IsBool();
	bool IsUndefined();
}

shared class AnyBase : IAny
{
	bool IsInt(){return false;};
	bool IsString(){return false;};
	bool IsBool(){return false;};
	bool IsUndefined(){return false;};
}

shared class AnyInt : AnyBase
{
	int val = 0;
	
	AnyInt(int val)
	{
		this.val = val;
	}
	
	bool IsInt(){return true;};
}

shared class AnyString : AnyBase
{
	string val = "";
	
	AnyString(string& val)
	{
		this.val = val;
	}
	
	bool IsString(){return true;};
}

shared class AnyBool : AnyBase
{
	bool val = false;
	
	AnyBool(bool val)
	{
		this.val = val;
	}
	
	bool IsBool(){return true;};
}

shared class AnyUndefined : AnyBase
{
	bool IsUndefined(){return true;};
}

shared interface IDynamicObject
{
	void SetInt(string& name, int& newint);
	void SetString(string& name, string& newstr);
	void SetBool(string& name, bool newbool);
	
	IAny@	GetInt(string& name);
	IAny@	GetString(string& name);
	IAny@ 	GetBool(string& name);
}

shared class DynamicObject : IDynamicObject
{
	dict<string, DynamicObject@>@ children;
	dict<string, AnyInt@>@ 		ints;
	dict<string, AnyString@>@ 	strings;
	dict<string, AnyBool@>@ 	bools;
	
	DynamicObject()
	{
		@this.children  = @dict<string, DynamicObject@>();
		@this.ints 		= @dict<string, AnyInt@>();
		@this.strings 	= @dict<string, AnyString@>();
		@this.bools 	= @dict<string, AnyBool@>();
	}
	
	void SetInt(string& name, int& newint)
	{
		if(this.ints.exists(name))
		{
			AnyInt@ existing = this.ints.get(name);
			existing.val = newint; 
		}
		else
		{
			AnyInt@ newAny = @AnyInt(newint);
			this.ints.set(name, @newAny);
		}
	}
	
	void SetString(string& name, string& newstr)
	{
		if(this.strings.exists(name))
		{
			AnyString@ existing = this.strings.get(name);
			existing.val = newstr; 
		}
		else
		{
			AnyString@ newAny = @AnyString(newstr);
			this.strings.set(name, @newAny);
		}
	}
	
	void SetBool(string& name, bool newbool)
	{
		if(this.bools.exists(name))
		{
			AnyBool@ existing = this.bools.get(name);
			existing.val = newbool; 
		}
		else
		{
			AnyBool@ newAny = @AnyBool(newbool);
			this.bools.set(name, @newAny);
		}
	}
	
	IAny@ GetInt(string& name)
	{
		if(this.ints.exists(name))
		{
			AnyInt@ existing = this.ints.get(name);
			return existing;
		}
		else
		{
			return null;
		}
	}
	
	IAny@ GetString(string& name)
	{
		if(this.strings.exists(name))
		{
			AnyString@ existing = this.strings.get(name);
			return existing;
		}
		else
		{
			return null;
		}
	}
	
	IAny@ GetBool(string& name)
	{
		if(this.bools.exists(name))
		{
			AnyBool@ existing = this.bools.get(name);
			return existing;
		}
		else
		{
			return null;
		}
	}
	
}


#endif