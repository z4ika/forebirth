// FOS Server
#ifndef SERVER_WORLDGEN
#define SERVER_WORLDGEN

LOG_MODULE_COMPILE

const string PATH 					= "GenerateWorld.json";
//time
const string KEY_TIME 				= "StartTime";
const string KEY_TIME_MULTIPLIER 	= "TimeMultiplier";
const string KEY_YEAR 				= "Year";
const string KEY_MONTH 				= "Month";
const string KEY_DAY 				= "Day";
const string KEY_HOUR 				= "Hour";
const string KEY_MINUTE				= "Minute";
const string KEY_SECOND 			= "Second";
//locations
const string KEY_LOCATIONS 			= "Locations";
const string KEY_FOLOC 				= "Foloc";
const string KEY_POSX 				= "PosX";
const string KEY_POSY 				= "PosY";


//default values
uint16 Multiplier = 1;
uint16 Year = 2246;
uint16 Month = 10;
uint16 Day = 30;
uint16 Hour = 1;
uint16 Minute = 0;
uint16 Second = 0;

shared class WorldgenLocation
{
	hash pid;
	string res_path;
	int posX;
	int posY;
	
	WorldgenLocation(hash pid, int posX, int posY)
	{
		this.pid = pid;
		this.posX = posX;
		this.posY = posY;
		this.res_path = GetHashStr(this.pid);
	}
	
	Location@ Spawn()
	{
		Location@ existing = GetLocationByPid(this.pid, 0); // if doesnt exist
		if(!valid(existing))
		{
			@existing = @CreateLocation(this.pid, this.posX, this.posY); // make it exist
		}
		else // or if exist, check for data
		{
			uint16 ex_x = existing.WorldX;
			uint16 ex_y = existing.WorldY;
			if( ( ex_x != uint16(this.posX) ) || ( ex_y != uint16(this.posY) ) ) // positions differ
			{
				@existing = @CreateLocation(this.pid, this.posX, this.posY);
			}
		}
		return (existing);
	}
}



dict<hash, WorldgenLocation@> WorldgenLocations = {};

bool Initialized 			= false;
bool InitializeError 		= false;
bool GenerateError 			= false;

bool ModuleInit()
{
	return Initialize();
}

bool Initialize()
{
	if(!Initialized)
	{
		Log("Initializing world gen...");
		//__IsFirstTime = false;
		Json::JSON@ config = Json::JSONLoad(PATH);
		if(!config.isUndefined())
		{
			
			//set starting time and time mult
			if(config.hasProperty(KEY_TIME)) 
			{
				Json::JSON@ timeData = config[KEY_TIME];
				if(timeData.hasProperty(KEY_TIME_MULTIPLIER))
				{
					timeData[KEY_TIME_MULTIPLIER] >> Multiplier;
				}
				if(timeData.hasProperty(KEY_YEAR))
				{
					timeData[KEY_YEAR] >> Year;
				}
				if(timeData.hasProperty(KEY_MONTH))
				{
					timeData[KEY_MONTH] >> Month;
				}
				if(timeData.hasProperty(KEY_DAY))
				{
					timeData[KEY_DAY] >> Day;
				}
				if(timeData.hasProperty(KEY_HOUR))
				{
					timeData[KEY_HOUR] >> Hour;
				}
				if(timeData.hasProperty(KEY_MINUTE))
				{
					timeData[KEY_MINUTE] >> Minute;
				}
				if(timeData.hasProperty(KEY_SECOND))
				{
					timeData[KEY_SECOND] >> Second;
				}
			}
			
			if(config.hasProperty(KEY_LOCATIONS))
			{
				Json::JSON@ locationsData = config[KEY_LOCATIONS];
				locationsData.forEach(@ProcessLocationsData);
			}
			
			if(!InitializeError)
			{
				Log("World gen initialized.");
				Initialized = !InitializeError;
			}
		}
		else
		{
			Log("Cannot open or json file is invalid at " + PATH + ", world gen init failed.");
			Initialized = false;
		}
	}
	return Initialized;
}

void ProcessLocationsData(Json::JSON@ value, const string& key)
{
	if(!value.isUndefined())
	{
		bool folocOk = value.hasProperty(KEY_FOLOC);
		bool posXOk	 = value.hasProperty(KEY_POSX);
		bool posYOk  = value.hasProperty(KEY_POSY);
		
		bool allOk = folocOk && posXOk && posYOk ;
		
		if(!allOk)
		{
			Log("Location generation data is missing " + (folocOk ? "" : KEY_FOLOC) + " " + (posXOk ? "" : KEY_POSX) + " " + (posYOk ? "" : KEY_POSY) + " at key of " + KEY_LOCATIONS + " " + key);
			InitializeError = true;
			return;
		}
		else // generate location
		{
			string temp = "";
			value[KEY_FOLOC] >> temp;
			if(temp != "")
			{
				hash locpid = GetStrHash(temp);
				int posX  = -1;
				int posY  = -1;
				value[KEY_POSX] >> posX;
				value[KEY_POSY] >> posY;
				if( ( posX >= 0) && (posY >= 0) )
				{
					
					WorldgenLocation@ worldloc = @WorldgenLocation(locpid, posX, posY);
					if(valid(worldloc))
					{
						AddWorldgenLocation(worldloc);
					}
					else
					{
						Log("Worldgen location unable to create."); // how ?
						InitializeError = true;
						return;
					}
				}
				else
				{
					Log("Negative position data of Foloc at key of " + KEY_LOCATIONS + " " + key);
					InitializeError = true;
					return;
				}
			}
			else
			{
				Log("Cannot read Foloc data at key of " + KEY_LOCATIONS + " " + key);
				InitializeError = true;
				return;
			}
		}
	}
}

bool Generate()
{
	SetTime( Multiplier, Year, Month, Day, Hour, Minute, Second );
	
	if(Initialized)
	{
		array<WorldgenLocation@>@ genLocs = GetWorldgenLocations();
		uint count = genLocs.length();
		for(uint i = 0; i < count; i++)
		{
			WorldgenLocation@ cur = genLocs[i];
			Log("Generating location " + cur.res_path);
			
			if(!valid(cur.Spawn()))
			{
				//do something if location was unable to spawn, should never happen though
				GenerateError = true;
			}
		}
	}
	else
		GenerateError = true;
	
	return !GenerateError;
}

void AddWorldgenLocation(WorldgenLocation& loc)
{
	hash locpid = loc.pid;
	if(WorldgenLocations.exists(locpid))
	{
		WorldgenLocations.remove(locpid);
	}
	
	WorldgenLocations.set(locpid, @loc);
}

WorldgenLocation@ GetWorldgenLocation(hash locpid)
{
	WorldgenLocation@ ptr = null;
	if(WorldgenLocations.exists(locpid))
	{
		@ptr = WorldgenLocations.get(locpid);
	}
	return ptr;
}

array<WorldgenLocation@>@ GetWorldgenLocations()
{
	uint locCount = WorldgenLocations.length();
	array<WorldgenLocation@>@ result = null; //;
	if(locCount > 0)
	{
		@result = @array<WorldgenLocation@>(locCount);
	}
	for(uint i = 0; i < locCount; i++)
	{
		@result[i] = WorldgenLocations.getValue(i);
	}
	return result;
}

void GetWorldStartDate(uint16& year, uint16& month, uint16& day, uint16& hour, uint16& minute, uint16& second)
{
	year 	= Year;
	month 	= Month;
	day 	= Day;
	hour 	= Hour;
	minute 	= Minute;
	second 	= Second;
}	
#endif
